import java.util.Scanner;
        
public class Game {
    Scanner kb = new Scanner(System.in);
    private Table table;
    private int row;
    private int col;
    private Player o;
    private Player x;
    
    public Game(){
        o = new Player('O');
        x = new Player('X');
    }
    
    public void startGame(){
        table = new Table(o,x);
    }
    
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    public void showTable(char[][] table){
        for(int i=0;i<table.length;i++){
            for(int j=0;j<table[i].length;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    
    public void showTurn(Player player){
        System.out.println("Turn "+player.getName());
    }
    
    public void showWin(Player player){
        System.out.println("Player "+player.getName()+" Win!!!");
    }
    
    public void showDraw(){
        System.out.println("Draw!!");
    }
    
    public void showBye(){
        System.out.println("Bye Bye");
    }
    
    public void showWrongInput(){
        System.out.println("Wrong position, Please input again!");
    }
    
    public void showStat(){
        System.out.println(o.getName()+":: Win:"+o.getWin()+" Lose:"+o.getLose()+" Draw:"+o.getDraw());
        System.out.println(x.getName()+":: Win:"+x.getWin()+" Lose:"+x.getLose()+" Draw:"+x.getDraw());
    }
    
    public boolean inputRowCol(){
        System.out.print("Please input row, col :"); 
        row = kb.nextInt();
        col = kb.nextInt();
        return true;
    }
    
    public boolean inputContinue(){
        while(true){
        System.out.print("Continue? (y/n):");
        char cont = kb.next().charAt(0);
        if(cont=='y'){
            return true;
        }else if (cont=='n'){
            return false;
        }else{
            continue;}
        }
    }
    
    public void showWelcomeAndTable(){
        showWelcome();
        startGame();
        showTable(table.getData()); 
    }
    
    public boolean checkWinAndShowWin(){
        if (table.checkWin(row-1,col-1)){
            showWin(table.getWinner());
            showStat();
            return true;
        }
        return false;
    }
    
    public boolean checkDrawAndShowDraw(){
        if (table.checkDraw()){
                showDraw();
                showStat();
            return true;
        }
        return false;
    }
    
    public void run(){
        showWelcomeAndTable();
        while(true){
            try{
            runOnce();
            }catch(Exception e){continue;}
            if (checkWinAndShowWin()||checkDrawAndShowDraw()){
                if(inputContinue()==false){break;
                }else{
                    showWelcomeAndTable();
                    continue;
                }
            }table.switchTurn();
        }showBye();
    }
    
    public void runOnce() throws Exception{
        try{
        showTurn(table.getcurrentPlayer());
        if(inputRowCol()){
           table.setRowCol(row, col);
        }
        }catch (Exception e){
            showWrongInput();
            throw e;
        }
        showTable(table.getData());    
    }
}
