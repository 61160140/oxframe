
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nutcha1223
 */
public class TestWriteFile {
    public static void main(String[] args) {
        File f = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        Player o = new Player('O');
        Player x = new Player('X');
        o.draw();
        x.draw();
        o.win();
        x.lose();
        o.win();
        x.lose();
        try {
            f = new File("ox.bin");
            fos = new FileOutputStream(f);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException ex) {
            }
        }

    }
}