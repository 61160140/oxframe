public class Table {
    private char[][] data;
    private Player currentPlayer;
    private Player win;
    private Player o;
    private Player x;
    private int count = 0;

    public Table(Player o, Player x) {
        data = new char[][]{{'-','-','-'},
                {'-','-','-'},
                {'-','-','-'}};
        this.o = o;
        this.x = x;
        randomTurn();
    }
    
    public void randomTurn(){
        if ((System.currentTimeMillis()%2)==0){
            currentPlayer = o;
        }else{
            currentPlayer = x;
        }
    }
    
    public boolean setRowCol(int row,int col){
        if(data[row-1][col-1]=='-'){
            data[row-1][col-1] = currentPlayer.getName();
            count++;
            return true;
        }else{
            return false;
        }
    }
    public boolean checkDraw(){
        if(count==9){
            o.draw();
            x.draw();
            return true;
        }
        return false;
    }
    
    public boolean checkWin(int row,int col){
        if (checkRow(row)||checkCol(col)||checkX1()||checkX2()){
            win = currentPlayer;
            if(currentPlayer==o){
                o.win();
                x.lose();
            }else{
                x.win();
                o.lose();
            }
            return true;
        }
        return false;
    }
    
    public void switchTurn(){
        if (currentPlayer == o){
            currentPlayer = x;
        }else{
            currentPlayer = o;
        }
    }
    
    public Player getcurrentPlayer(){
        return currentPlayer;
    }
    
    public char[][] getData(){
        return data;
    }
    
    private boolean checkRow(int row){
        if(data[row][0]==data[row][1] && data[row][0]==data[row][2] && data[row][0]==currentPlayer.getName()){
                return true;
        }
        return false;
    }
    
    private boolean checkCol(int col){
        if(data[0][col]==data[1][col] && data[0][col]==data[2][col] && data[0][col]==currentPlayer.getName()){
                return true;
        }
        return false;
    }
    
    private boolean checkX1(){
        if(data[0][0]==data[1][1] && data[0][0]==data[2][2] && data[0][0]==currentPlayer.getName()){
            return true;
        }
        return false;
    }
    
    private boolean checkX2(){
        if(data[0][2]==data[1][1] && data[0][2]==data[2][0] && data[0][2]==currentPlayer.getName()){
            return true;
        }
        return false;
    }
    
    public Player getWinner(){
        return win;
    }
}
